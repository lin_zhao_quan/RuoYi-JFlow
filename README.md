# [RuoYi平台简介](http://www.ruoyi.vip/)

RuoYi是一个后台管理系统，基于经典技术组合（Spring Boot、Apache Shiro、MyBatis、Thymeleaf）主要目的让开发者注重专注业务，降低技术难度，从而节省人力成本，缩短项目周期，提高软件安全质量。

[开发文档](http://doc.ruoyi.vip/)


# [JFlow工作流](http://ccflow.org/)

驰骋工作流引擎研发与2003年，具有.net与java两个版本，这两个版本代码结构，数据库结构，设计思想，功能组成， 操作手册，完全相同。 导入导出的流程模版，表单模版两个版本完全通用

CCFlow是.net版本的简称，由济南驰骋团队负责研发，JFlow是java版本的简称，在CCFlow的基础上升级改造而来，公司联合易科德软件共同研发。两款产品向社会100%开源。

[开发文档](http://ccbpm.mydoc.io/) 
 

## 开始集成准备

 1. 下载两个开源项目的源码

RuoYi git地址:  [https://gitee.com/y_project/RuoYi.git](https://gitee.com/y_project/RuoYi.git)

JFlow工作流(springboot版本) git地址: [https://gitee.com/opencc/JFlowSpringBoot.git](https://gitee.com/opencc/JFlowSpringBoot.git)

 2. 数据库准备

建立一个空数据库(如ry) 导入RuoYi  sql文件  

建立一个空数据库(如jflow) JFlow工作流  
修改配置文件,启动项目 
直接访问 :http://localhost:8089/jflow-web/WF/Admin/DBInstall.htm
同步数据库
 ​​![在这里插入图片描述](https://img-blog.csdnimg.cn/20200218114352859.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2tpa29jaw==,size_16,color_FFFFFF,t_70)
 同步数据库完成
 ![在这里插入图片描述](https://img-blog.csdnimg.cn/20200218114557636.png)

**

> ps:idea如果出现404错误  请修改idea配置如图   
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200218115006259.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2tpa29jaw==,size_16,color_FFFFFF,t_70)
> 数据库表大小写不区分配置
> ![在这里插入图片描述](https://img-blog.csdnimg.cn/20200219092233722.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2tpa29jaw==,size_16,color_FFFFFF,t_70)

**
##  RuoYi与JFlow组织架构集成(JFlow的组织机构集成，就是删除JFlow的组织机构表，建立同数据结构一样的视图!!!)
JFlow组织机构表
![jflow组织架构表](https://img-blog.csdnimg.cn/20200218115944856.png)
在RuoYi数据库视图中建立和JFlow组织结构相同名的视图

 1. 将JFlow组织机构表重命名
  ![在这里插入图片描述](https://img-blog.csdnimg.cn/20200218120545909.png)
  
 2. 因为没有相同的表所以先将数据库表合并为一个数据库
 3. 建立视图
 .![在这里插入图片描述](https://img-blog.csdnimg.cn/20200218120729219.png)

> RuoYi用户,部门,岗位与JFlow组织机构表对应增加关联,建立视图 
> ps: jflow数据库
> 同步后数据库

## JFlow 核心JFlow-core整合
增加一个模块按照JFlowSpringBoot源码中jflow-core模块放入项目
![在这里插入图片描述](https://img-blog.csdnimg.cn/2020022014282034.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2tpa29jaw==,size_16,color_FFFFFF,t_70)
在admin模块增加和修改配置文件
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200220142933649.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2tpa29jaw==,size_16,color_FFFFFF,t_70)
将JFlowSpringBoot中jflow-web 配置文件整合到Admin模块

## JFlow 核心JFlow-web整合
将JFlow-web 中页面文件 复制到admin模块
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200220143307948.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2tpa29jaw==,size_16,color_FFFFFF,t_70)
修改配置文件直接访问此文件夹下静态文件
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200220143417816.png)

##  RuoYi增加菜单访问JFlow工作流
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200220144041116.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2tpa29jaw==,size_16,color_FFFFFF,t_70)
菜单链接如上图

## 整合其他问题处理和注意事项

 1. 静态资源映射JFlow页面文件必须配置
 2. JFlow配置文件修改 (修改数据库配置,扫包)
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200220144353937.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2tpa29jaw==,size_16,color_FFFFFF,t_70) 
 3. JFlow  excel工具 版本问题
 	jflow  Excel表导出  版本为3.11  RuoYi版本为 3.17 (修改BP.Difference.Handler.WebContralBase文件)
 	![在这里插入图片描述](https://img-blog.csdnimg.cn/20200220144933375.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2tpa29jaw==,size_16,color_FFFFFF,t_70)
 4. 如果出现此错误-------在配置文件jflow.properties中增加ServicePath配置项
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200220145103549.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2tpa29jaw==,size_16,color_FFFFFF,t_70)
可在此修改 或者在配置文件jflow.properties中增加ServicePath配置指向JFlow静态文件夹
 5. 此次初步集成~流程设计器部分功能不可用,其他功能和bug为测试

#####   [数据库文件下载](https://download.csdn.net/download/kikock/12174581)
