package BP.GPM;

import BP.DA.*;
import BP.En.*;
import BP.En.Map;

import java.util.*;

/**
 * 岗位类型
 */
public class StationType extends EntityNoName {

    /**
     * 岗位类型
     */
    public StationType() {
    }

    /**
     * 岗位类型
     *
     * @param _No
     * @throws Exception
     */
    public StationType(String _No) throws Exception {
        super(_No);
    }

    ///#region 属性
    public final String getFK_StationType() throws Exception {
        return this.GetValStrByKey(StationAttr.FK_StationType);
    }


    ///#endregion

    public final void setFK_StationType(String value) throws Exception {
        this.SetValByKey(StationAttr.FK_StationType, value);
    }

    ///#endregion


    ///#region 构造方法

    public final String getFK_StationTypeText() throws Exception {
        return this.GetValRefTextByKey(StationAttr.FK_StationType);
    }

    /**
     * 岗位类型Map
     */
    @Override
    public Map getEnMap() {
        if (this.get_enMap() != null) {
            return this.get_enMap();
        }
        Map map = new Map("Port_StationType", "岗位类型");
        map.Java_SetCodeStruct("2");

        map.Java_SetDepositaryOfEntity(Depositary.None);
        map.Java_SetDepositaryOfMap(Depositary.Application);

        map.AddTBStringPK(StationTypeAttr.No, null, "编号", true, true, 2, 2, 2);
        map.AddTBString(StationTypeAttr.Name, null, "名称", true, false, 1, 50, 20);
        map.AddTBInt(StationTypeAttr.Idx, 0, "顺序", true, false);
        map.AddTBString(StationTypeAttr.OrgNo, null, "组织机构编号", true, false, 0, 50, 20);

        this.set_enMap(map);
        return this.get_enMap();
    }

    ///#endregion

    ///#region 实现基本的方方法
    @Override
    public UAC getHisUAC() throws Exception {
        UAC uac = new UAC();
        uac.OpenForSysAdmin();
        return uac;
    }
}
