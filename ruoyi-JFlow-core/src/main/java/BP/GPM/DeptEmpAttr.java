package BP.GPM;

import BP.DA.*;
import BP.En.*;

import java.util.*;

/**
 * 部门人员信息
 */
public class DeptEmpAttr {

    ///#region 基本属性
    /**
     * 部门
     */
    public static final String FK_Dept = "FK_Dept";
    /**
     * 人员
     */
    public static final String FK_Emp = "FK_Emp";

    ///#endregion
}
