package BP.Web;

public enum UserWorkDev {
    PC,
    Mobile,
    TablePC;

    public static UserWorkDev forValue(int value) {
        return values()[value];
    }

    public int getValue() {
        return this.ordinal();
    }
}
