package BP.Sys;

import java.util.List;

import BP.En.*;

/**
 * Excel数据表集合
 */
public class ExcelTables extends EntitiesNoName {

    ///#region 属性
    ///#region 构造方法
    public ExcelTables() {
    }

    ///#endregion 属性


    public ExcelTables(String fk_excelfile) throws Exception {
        this.Retrieve(ExcelTableAttr.FK_ExcelFile, fk_excelfile, ExcelTableAttr.Name);
    }

    /**
     * 生成Excel数据表实体
     */
    @Override
    public Entity getNewEntity() {
        return new ExcelTable();
    }

    public final List<ExcelTable> ToJavaList() {
        return (List<ExcelTable>) (Object) this;
    }
    ///#endregion 构造方法
}
