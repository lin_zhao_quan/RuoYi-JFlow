package BP.Sys;

import BP.DA.*;
import BP.En.*;

import java.util.*;

/**
 * 属性
 */
public class ContrastAttr {
    /**
     * 对比项目
     */
    public static final String ContrastKey = "ContrastKey";
    /**
     * KeyVal1
     */
    public static final String KeyVal1 = "KeyVal1";
    /**
     * KeyVal2
     */
    public static final String KeyVal2 = "KeyVal2";
    /**
     * 分类条件
     */
    public static final String SortBy = "SortBy";
    /**
     * 对比的值
     */
    public static final String KeyOfNum = "KeyOfNum";
    /**
     * 分组方式
     */
    public static final String GroupWay = "GroupWay";
    /**
     * 排序
     */
    public static final String OrderWay = "OrderWay";
}
