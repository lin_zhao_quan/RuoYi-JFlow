package BP.Sys;

import BP.DA.*;
import BP.En.*;
import BP.*;

import java.util.*;

/**
 * 百分比显示方式
 */
public enum PercentModel {
    /**
     * 不显示
     */
    None,
    /**
     * 纵向
     */
    Vertical,
    /**
     * 横向
     */
    Transverse;

    public static final int SIZE = java.lang.Integer.SIZE;

    public static PercentModel forValue(int value) {
        return values()[value];
    }

    public int getValue() {
        return this.ordinal();
    }
}
