package BP.En;

import BP.DA.*;

/**
 * NoEntity 的摘要说明。
 */
public abstract class EntityMyPK extends Entity {

    ///#region 构造
    public EntityMyPK() {

    }

    /**
     * class Name
     *
     * @param _MyPK _MyPK
     * @throws Exception
     */
    protected EntityMyPK(String _MyPK) throws Exception {
        this.setMyPK(_MyPK);
        this.Retrieve();
    }

    ///#region 基本属性
    @Override
    public String getPK() {
        return "MyPK";
    }

    /**
     * @return
     * @throws Exception
     */
    public String InitMyPKVals() throws Exception {
        return this.getMyPK();
    }

    ///#endregion

    /**
     * 集合类名称
     *
     * @throws Exception
     */
    public final String getMyPK() throws Exception {
        return this.GetValStringByKey(EntityMyPKAttr.MyPK);
    }

    public final void setMyPK(String value) throws Exception {
        this.SetValByKey(EntityMyPKAttr.MyPK, value);
    }

    ///#endregion
}
