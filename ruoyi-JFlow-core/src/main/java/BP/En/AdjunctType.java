package BP.En;

import BP.DA.*;
import BP.Sys.*;
import BP.Web.Controls.*;

import java.time.*;
import java.math.*;

/**
 * 附件类型
 */
public enum AdjunctType {
    /**
     * 不需要附件。
     */
    None,
    /**
     * 图片
     */
    PhotoOnly,
    /**
     * word 文档。
     */
    WordOnly,
    /**
     * 所有的类型
     */
    ExcelOnly,
    /**
     * 所有的类型。
     */
    AllType;

    public static final int SIZE = java.lang.Integer.SIZE;

    public static AdjunctType forValue(int value) {
        return values()[value];
    }

    public int getValue() {
        return this.ordinal();
    }
}
