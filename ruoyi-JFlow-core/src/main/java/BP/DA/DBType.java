package BP.DA;

/**
 * 数据库类型
 */
public enum DBType {
    /**
     * sqlserver
     */
    MSSQL(0),
    /**
     * oracle
     */
    Oracle(1),
    /**
     * Access
     */
    Access(2),
    /**
     * PostgreSQL
     */
    PostgreSQL(3),
    /**
     * DB2
     */
    DB2(4),
    /**
     * MySQL
     */
    MySQL(5),
    /**
     * Informix
     */
    Informix(6),
    /*
     * 达梦
     * */
    DM(7);


    public static final int SIZE = java.lang.Integer.SIZE;
    private static java.util.HashMap<Integer, DBType> mappings;
    private int intValue;

    private DBType(int value) {
        intValue = value;
        getMappings().put(value, this);
    }

    private static java.util.HashMap<Integer, DBType> getMappings() {
        if (mappings == null) {
            synchronized (DBType.class) {
                if (mappings == null) {
                    mappings = new java.util.HashMap<Integer, DBType>();
                }
            }
        }
        return mappings;
    }

    public static DBType forValue(int value) {
        return getMappings().get(value);
    }

    public int getValue() {
        return intValue;
    }
}
