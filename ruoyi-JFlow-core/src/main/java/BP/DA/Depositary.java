package BP.DA;

/**
 * 保管位置
 */
public enum Depositary {
    /**
     * 不保管
     */
    None,
    /**
     * 全体
     */
    Application;

    public static final int SIZE = java.lang.Integer.SIZE;

    public static Depositary forValue(int value) {
        return values()[value];
    }

    public int getValue() {
        return this.ordinal();
    }
}
