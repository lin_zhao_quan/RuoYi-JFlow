package BP.WF.Rpt;

import BP.WF.*;

import java.util.*;
import java.io.*;
import java.time.*;

public enum FillDirection {
    Vertical(1),

    Horizontal(2);

    public static final int SIZE = java.lang.Integer.SIZE;
    private static java.util.HashMap<Integer, FillDirection> mappings;
    private int intValue;

    private FillDirection(int value) {
        intValue = value;
        getMappings().put(value, this);
    }

    private static java.util.HashMap<Integer, FillDirection> getMappings() {
        if (mappings == null) {
            synchronized (FillDirection.class) {
                if (mappings == null) {
                    mappings = new java.util.HashMap<Integer, FillDirection>();
                }
            }
        }
        return mappings;
    }

    public static FillDirection forValue(int value) {
        return getMappings().get(value);
    }

    public int getValue() {
        return intValue;
    }
}
