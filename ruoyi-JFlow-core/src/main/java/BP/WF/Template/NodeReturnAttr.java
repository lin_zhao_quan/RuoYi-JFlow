package BP.WF.Template;

import BP.DA.*;
import BP.En.*;
import BP.Port.*;
import BP.WF.*;

import java.util.*;

//using BP.ZHZS.Base;


/**
 * 可退回的节点属性
 */
public class NodeReturnAttr {
    /**
     * 节点
     */
    public static final String FK_Node = "FK_Node";
    /**
     * 退回到
     */
    public static final String ReturnTo = "ReturnTo";
    /**
     * 中间点
     */
    public static final String Dots = "Dots";
}
