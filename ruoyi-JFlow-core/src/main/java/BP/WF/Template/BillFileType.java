package BP.WF.Template;

import BP.DA.*;
import BP.En.*;
import BP.WF.*;
import BP.WF.*;

import java.util.*;
import java.io.*;

/**
 * 生成的类型
 */
public enum BillFileType {
    /**
     * Word
     */
    Word(0),
    PDF(1),
    Excel(2),
    Html(3),
    RuiLang(5);

    public static final int SIZE = java.lang.Integer.SIZE;
    private static java.util.HashMap<Integer, BillFileType> mappings;
    private int intValue;

    private BillFileType(int value) {
        intValue = value;
        getMappings().put(value, this);
    }

    private static java.util.HashMap<Integer, BillFileType> getMappings() {
        if (mappings == null) {
            synchronized (BillFileType.class) {
                if (mappings == null) {
                    mappings = new java.util.HashMap<Integer, BillFileType>();
                }
            }
        }
        return mappings;
    }

    public static BillFileType forValue(int value) {
        return getMappings().get(value);
    }

    public int getValue() {
        return intValue;
    }
}
