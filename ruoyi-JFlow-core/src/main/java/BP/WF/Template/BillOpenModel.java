package BP.WF.Template;

import BP.DA.*;
import BP.En.*;
import BP.WF.*;
import BP.WF.*;

import java.util.*;
import java.io.*;

/**
 * 生成的文件打开方式
 */
public enum BillOpenModel {
    /**
     * 下载保存
     */
    DownLoad(0),
    /**
     * 在线WebOffice打开
     */
    WebOffice(1);

    public static final int SIZE = java.lang.Integer.SIZE;
    private static java.util.HashMap<Integer, BillOpenModel> mappings;
    private int intValue;

    private BillOpenModel(int value) {
        intValue = value;
        getMappings().put(value, this);
    }

    private static java.util.HashMap<Integer, BillOpenModel> getMappings() {
        if (mappings == null) {
            synchronized (BillOpenModel.class) {
                if (mappings == null) {
                    mappings = new java.util.HashMap<Integer, BillOpenModel>();
                }
            }
        }
        return mappings;
    }

    public static BillOpenModel forValue(int value) {
        return getMappings().get(value);
    }

    public int getValue() {
        return intValue;
    }
}
