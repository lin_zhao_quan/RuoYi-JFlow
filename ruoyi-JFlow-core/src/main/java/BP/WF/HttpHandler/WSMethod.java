package BP.WF.HttpHandler;

import BP.DA.*;
import BP.Port.*;
import BP.En.*;
import BP.Tools.*;
import BP.WF.*;
import BP.Web.*;
import BP.Sys.*;
import BP.WF.Template.*;
import BP.WF.*;

import java.util.*;

public class WSMethod {
    private String No;
    private String Name;
    private HashMap<String, String> ParaMS;
    private String Return;

    public final String getNo() {
        return No;
    }

    public final void setNo(String value) {
        No = value;
    }

    public final String getName() {
        return Name;
    }

    public final void setName(String value) {
        Name = value;
    }

    public final HashMap<String, String> getParaMS() {
        return ParaMS;
    }

    public final void setParaMS(HashMap<String, String> value) {
        ParaMS = value;
    }

    public final String getReturn() {
        return Return;
    }

    public final void setReturn(String value) {
        Return = value;
    }
}
